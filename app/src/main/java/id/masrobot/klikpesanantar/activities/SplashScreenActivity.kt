package id.masrobot.klikpesanantar.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.masrobot.klikpesanantar.R

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val mWindow = window
        mWindow.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }
}
