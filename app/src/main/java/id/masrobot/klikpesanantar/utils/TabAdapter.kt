package id.masrobot.klikpesanantar.utils

import androidx.fragment.app.FragmentPagerAdapter

class TabAdapter(fm: androidx.fragment.app.FragmentManager): FragmentPagerAdapter(fm) {

    private val fragment: ArrayList<androidx.fragment.app.Fragment> = ArrayList()
    private val fragTitle: ArrayList<String> = ArrayList()

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return fragment[position]
    }

    override fun getCount(): Int {
        return fragment.size
    }

    fun addFragment(frag: androidx.fragment.app.Fragment, title: String) {
        fragment.add(frag)
        fragTitle.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragTitle[position]
    }
}
